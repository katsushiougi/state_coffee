module.exports = (grunt) ->

  grunt.initConfig
    pkg : grunt.file.readJSON('package.json')

    banner: """
/*! <%= pkg.name %> (<%= pkg.repository.url %>)
 * lastupdate: <%= grunt.template.today("yyyy-mm-dd") %>
 * version: <%= pkg.version %>
 * author: <%= pkg.author.name %>
 * url: <%= pkg.author.url %>
 * License: MIT */

"""
    connect:
      server:
        options:
          port: 9001
          base: "<%= pkg.destdir %>"
          hostname: "0.0.0.0"
        middleware: (connect, options) ->
          [lrSnippet, folderMount(connect, '.')]

    notify:
      success:
        options:
          title: 'SUCCESS'
          message: '＼(^o^)／'

    bower:
      install:
        options:
          targetDir: "./libs"
          layout: "byType"
          install: true
          verbose: false
          cleanTargetDir: true
          cleanBowerDir: true

    coffee:
      dist:
        options:
          join: true
          # sourcemap: true
        src: [
          "src/coffee/*/*.coffee"
          "src/coffee/*.coffee"
        ]
        dest: "<%= pkg.destdir %>common/js/scripts.js"

    concat:
      all:
        options:
          banner: '<%= banner %>'
        src: [
          "libs/js/jquery/jquery.min.js"
          "<%= coffee.dist.dest %>"
        ]
        dest: "<%= coffee.dist.dest %>"

    copy:
      init:
        files:[
          {
            expand: true
            cwd: "libs/scss/bourbon"
            src: "**/*"
            dest: "src/scss/bourbon"
          }
          {
            expand: true
            cwd: "libs/scss/my.scss"
            src: "**/*"
            dest: "src/scss/libs"
          }
          # Use Fancybox
          # {
            # expand: true
            # cwd: "libs/fancybox/fancybox"
            # src: "**/*"
            # dest: "../common/fancybox"
          # }
        ]

    shell:
      clean:
        command:
          "rm -rf src/scss/sprites"

    watch:
      all:
        files: [
          "<%= coffee.dist.src %>"
        ]
        tasks: [ "default" ]
        options:
          livereload: true
          nospawn: true
      coffee:
        files: [ "<%= coffee.dist.src %>" ]
        tasks: ["coffeebuild"]
        options:
          livereload: true
          nospawn: true

  pkg = grunt.file.readJSON('package.json')
  for taskName of pkg.devDependencies
    if taskName.substring(0, 6) == 'grunt-' then grunt.loadNpmTasks(taskName)

  grunt.registerTask "init", [
    "bower:install"
    "copy:init"
  ]

  grunt.registerTask "w", [
    "connect"
    "watch:all"
  ]

  # ============================================================
  # Default Build

  grunt.registerTask "default", [
    "coffee:dist"
    "concat"
    "notify:success"
  ]
  

