require.config({
  baseUrl: "js",
  paths: {
    "jquery": "../../libs/js/jquery/jquery.min",
    "backbone": "../../libs/js/backbone/backbone-min",
    "underscore": "../../libs/js/underscore/underscore-min"
  },
  shim: {
    "backbone": {
      deps: ["jquery", "underscore"],
      exports: "Backbone"
    },
    "underscore": {
      exports: "_"
    }
  }
});
