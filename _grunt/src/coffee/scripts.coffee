do ($=jQuery, window=window, document=document, ls=localStorage) ->

  $window = $(window)
  ns = {}

  state = null
  selected = null

  transtState = (nextState) ->
    $("#debug").text state + " -> " + nextState
    state = nextState

  # ============================================================
  # AbstractState

  class ns.AbstractState
    constructor: (e) ->
    initialize: (e) ->
    onMouseDown: (e) ->
    onMouseMove: (e) ->
    onMouseUp: (e) ->
    onDblClick: (e) ->
    onKeyDown: (e) ->
    toString: (e) -> return "AbstractState"

  # ============================================================
  # DragMovingState

  class ns.DragMovingState extends ns.AbstractState

    constructor: (target) ->
      selected = target
      $("#drag-item").text($(selected).text()).show()

    onMouseMove: (e) ->
      target = e.target
      if target.tagName == "LI"
        $("#drag-item").css {left: e.pageX + 2, top: e.pageY + 2}
        $("#list li").removeClass "drag-over"
        $(target).addClass "drag-over"

    onMouseUp: (e) ->
      target = e.target
      $("#list li").removeClass "drag-over"
      if target != selected then $(target).after selected
      $("#drag-item").hide()

  # ============================================================
  # NormalState

  class ns.NormalState extends ns.AbstractState

    constructor: ->
      selected = null

    onMouseDown: (e) ->
      target = e.target
      if target.tagName == "LI" then transtState new ns.DragMovingState target

    toString: (e) -> return "NormalState"

  # ============================================================
  # MyUI

  class ns.MyUI

    constructor: ->

      transtState new ns.NormalState()

      $("body").on "sekectStart", () => return false;
      $("body").on "mousedown", (e) => e.preventDefault()

      $("#list li").mousedown (e) => state.onMouseDown e
      $("#list").mousemove (e) => state.onMouseMove e
      $("#list").mouseup (e) => state.onMouseUp e
      $("#list").dblclick (e) => state.onDblClick e
      $(document).keydown (e) => state.onKeyDown e  

  $ ->
    new ns.MyUI